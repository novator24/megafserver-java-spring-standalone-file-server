# megafserver-java-spring-standalone-file-server

## Overview

MegaFServer - Java Spring StandAlone File Server as RESTful API

## QuickStart

./build.sh && ./start.sh

## RestAPI

http://localhost:8080 => this help

/time.json => current time

/index.json => topics list

/current.json => current instants of runs for each topic

/stats.json => current stats for each topic

/values.json => current values for each topic

## Description RU

Техническое Задание.

Необходимо разработать веб-сервис и оформить его в виде приложения для linux:

- Веб-сервис самый простой, например на запрос ping отвечает pong, выдает текущую дату и время.

- Веб-сервис должен запускаться из standalone jar, без использования application server. Мы рекомендуем spring boot (http://projects.spring.io/spring-boot/).

- Результатом разработки должен быть пакет для инсталляции, то есть некоторая сборка кода и инсталлер, после применения которого сервис запускается на linux, и оформлен по стандартным практикам linux: исполняемые файлы, логи, pid-файлы лежат по стандартным директориям. 

Сервис должен запускаться автоматически при рестарте сервера.

- Использовать любую версию linux (какая ему удобнее, deb-based или rpm-based), любой формат поставки сборки и любой инсталлер (написать самому или использовать готовый).

- Предоставить репозиторий кода на github/bitbucket с кодом самого сервиса, сборочного процесса и инсталлера.

Дополнительное задание.

Дано:
Директория, в которой лежат файлы по следующей структуре: <topic_name>/history/<run_timestamp>/offsets.csv
<topic_name> - имя топика (по сути просто строка) <run_timestamp> - таймстемп запуска некоей процедуры, в формате YYYY-MM-DD-HH-mm-ss (минуты и секунды с лидирующими нулями) offsets.csv - файл с двумя колонками, разделенными запятыми. Первая колонка - номер партиции (int), вторая колонка - число сообщений (long). В одном файле десятки записей.

Нужно разработать REST-сервис, который позволяет получить:
- Список топиков, данные по которым есть данные в этой директории
- По каждому топику, таймстемп последнего запуска
- По каждому топику, для последнего запуска, статистику: суммарное число сообщений во всех патрициях, максимальное/минимальное число сообщений, среднее число сообщений
- По каждому топику, список партиций и число сообщений по каждой партиции во время последнего запуска

Сервис должен поставляться в виде запускаемого jar-файла, параметром запуска сервиса должен быть base_dir - директория откуда начинать искать топики. Сборка через maven, контейнер для приложения - http://projects.spring.io/spring-boot/.

## Build

To build megafserver-java-spring-standalone-file-server as a war file, run:

```
mvn clean package
```

## Run

To start the application, run:

```
mvn spring-boot:run
```

## Verify

To verify the application is running, visit:

```
http://localhost:8080
```

