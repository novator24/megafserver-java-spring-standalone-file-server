import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import java.nio.charset.StandardCharsets;
import java.io.File;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Date;
//import java.io.Stream;
import java.lang.Exception;
import java.io.IOException;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import java.util.concurrent.atomic.AtomicInteger;

// ChangeLog of beta version:
// 0) async requests processing
// 1) NIO selectors (watchers) 
// 2) cache requests

@RestController
@EnableAutoConfiguration
public class MegaFServer implements CommandLineRunner {
    private static final Logger logger = 
            Logger.getLogger(MegaFServer.class);

    @RequestMapping("/")
    String requestHome() {
        return processHome();
    }

    @RequestMapping("/time.json")
    String requestTime() {
        return processTime();
    }

    @RequestMapping("/index.json")
    String requestIndex() {
        int index = cache.curBufIndex.get();
        cache.readers[index].incrementAndGet();
        String response =
            (0 == cache.writers[index].get()) ?
            cache.indexBuffers[index] : processIndex(false);
        cache.readers[index].decrementAndGet();
        return response;
    }

    @RequestMapping("/current.json")
    String requestCurrent() {
        int index = cache.curBufIndex.get();
        cache.readers[index].incrementAndGet();
        String response =
            (0 == cache.writers[index].get()) ?
            cache.currentBuffers[index] : processCurrent(false);
        cache.readers[index].decrementAndGet();
        return response;
    }

    @RequestMapping("/stats.json")
    String requestStats() {
        int index = cache.curBufIndex.get();
        cache.readers[index].incrementAndGet();
        String response =
            (0 == cache.writers[index].get()) ?
            cache.statsBuffers[index] : processStats(false);
        cache.readers[index].decrementAndGet();
        return response;
    }

    @RequestMapping("/values.json")
    String requestValues() {
        int index = cache.curBufIndex.get();
        cache.readers[index].incrementAndGet();
        String response =
            (0 == cache.writers[index].get()) ?
            cache.valuesBuffers[index] : processValues(false);
        cache.readers[index].decrementAndGet();
        return response;
    }

    private class MegaFCache {
        public String[] indexBuffers = new String[2];
        public String[] currentBuffers = new String[2];
        public String[] statsBuffers = new String[2];
        public String[] valuesBuffers = new String[2];
        public AtomicInteger[] readers = new AtomicInteger[2];
        public AtomicInteger[] writers = new AtomicInteger[2];
        public AtomicInteger curBufIndex = new AtomicInteger(0);
        public MegaFCache() {
            indexBuffers[0] = new String();
            indexBuffers[1] = new String();
            currentBuffers[0] = new String();
            currentBuffers[1] = new String();
            statsBuffers[0] = new String();
            statsBuffers[1] = new String();
            valuesBuffers[0] = new String();
            valuesBuffers[1] = new String();
            readers[0] = new AtomicInteger(0);
            readers[1] = new AtomicInteger(0);
            writers[0] = new AtomicInteger(0);
            writers[1] = new AtomicInteger(0);
        }
    }

    public void updateCache() {
        int index = (0 == cache.curBufIndex.get()) ? 1 : 0;
        cache.writers[index].incrementAndGet();
        while (cache.readers[index].get() != 0) {
            try {
                Thread.sleep(50);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
        cache.indexBuffers[index] = processIndex(true);
        cache.currentBuffers[index] = processCurrent(true);
        cache.statsBuffers[index] = processStats(true);
        cache.valuesBuffers[index] = processValues(true);
        if (0 == index) {
           cache.curBufIndex.decrementAndGet();
        } else {
           cache.curBufIndex.incrementAndGet();
        }
        cache.writers[index].decrementAndGet();
    }

    MegaFCache cache = new MegaFCache();
    MegaFBackground background = null;
    ThreadPoolTaskExecutor executor = null;
    private String workDir = null;

    private class MegaFBackground {
        private class WatcherTask implements Runnable {
            public void run() {
                WatchService watcher = null;
                Path dir = null;
                try {
                    watcher = FileSystems.getDefault().newWatchService();
                    dir = Paths.get(workDir);
                    WatchKey key = dir.register(watcher,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE,
                    StandardWatchEventKinds.ENTRY_MODIFY);
                } catch (IOException x) {
                    logger.error(x);
                    return;
                }
                while (true) {
                    // wait for key to be signaled
                    WatchKey key;
                    try {
                        key = watcher.take();
                    } catch (InterruptedException x) {
                        return;
                    }
                    for (WatchEvent<?> event: key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();
                        if (kind == StandardWatchEventKinds.OVERFLOW) {
                            continue;
                        }
                        WatchEvent<Path> ev = (WatchEvent<Path>)event;
                        Path filename = ev.context();
                        logger.debug("Updated file " + filename.toString());
                        updateCache();
                        logger.debug("Updated cache");
                    }
                    boolean valid = key.reset();
                    if (!valid) {
                        break;
                    }
                }
            }
        }
        private TaskExecutor taskExecutor;
        public MegaFBackground(TaskExecutor taskExecutor) {
            this.taskExecutor = taskExecutor;
            taskExecutor.execute(new WatcherTask());
        }
    }

    public void run(String[] args) throws Exception {
        if (0 == args.length) {
            workDir = ".";
        } else
        if (1 == args.length) {
            workDir = args[0];
        } else {
            throw new Exception("Error: too many arguments are set." +
                " Usage: java -jar megaserver-1.0.jar [workdir]");
        }
        updateCache();
        if (background == null) {
            executor = new ThreadPoolTaskExecutor();
            executor.setCorePoolSize(5);
            executor.setMaxPoolSize(10);
            executor.setQueueCapacity(10);
            executor.initialize();
            background = new MegaFBackground(executor);
        }
    }

    String processHome() {
        String response = "<html><body>RestAPI:<br/>" +
            "  /time.json => current time<br/>" +
            "  /index.json => topics list<br/>" +
            "  /current.json => current instants of runs for each topic<br/>" +
            "  /stats.json => current stats for each topic<br/>" +
            "  /values.json => current values for each topic</body></html>\n";
        logger.debug("/ done");
        return response;
    }

    String processTime() {
        Date dt = new Date();
        String stamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(dt);
        String response = "{\"time\": " + quote(stamp) + "}";
        logger.debug("time.json done");
        return response;
    }

    String processIndex(boolean background) {
        String json = "{\"service\": " + quote("index") + ", " +
            "\"dir\": " + quote(workDir);
        String response = json + ", \"result\": [";
        try {
            if (!background) {
                throw new IOException("processing not finished yet");
            }
            List<Path> topics = readDirs(workDir);
            boolean needComma = false;
            for (Iterator<Path> iter = topics.iterator(); iter.hasNext(); ) {
                if (needComma) {
                    response += ", " +
                        quote(iter.next().getFileName().toString());
                } else {
                    response += quote(iter.next().getFileName().toString());
                    needComma = true;
                }
            }
        } catch(IOException ex) {
            logger.error("index.json " + ex.toString());
            return json + ", \"error\": " + quote(ex.toString()) + "}";
        }
        logger.debug("index.json done");
        return response + "]}";
    }

    String processCurrent(boolean background) {
        String json = "{\"service\": " + quote("current") + ", " +
            "\"dir\": " + quote(workDir);
        String response = json + ", \"result\": {";
        try {
            if (!background) {
                throw new IOException("processing not finished yet");
            }
            List<Path> topics = readDirs(workDir);
            boolean needComma = false;
            for (Iterator<Path> iter = topics.iterator(); iter.hasNext(); ) {
                Path cur = iter.next();
                List<String> runs = readSortedRuns(cur.toString());
                if (0 != runs.size()) {
                    String chosen = runs.get(runs.size() - 1);
                    if (needComma) {
                        response += ", " +
                            quote(cur.getFileName().toString()) +
                            ": " + quote(chosen);
                    } else {
                        response += quote(cur.getFileName().toString()) +
                            ": " + quote(chosen);
                        needComma = true;
                    }
                }
            }
        } catch (IOException ex) {
            logger.error("current.json " + ex.toString());
            return json + ", \"error\": " + quote(ex.toString()) + "}";
        }
        logger.debug("current.json done");
        return response + "}}";
    }

    String processStats(boolean background) {
        String json = "{\"service\": " + quote("stats") + ", " +
            "\"dir\": " + quote(workDir);
        String response = json + ", \"result\": {";
        try {
            if (!background) {
                throw new IOException("processing not finished yet");
            }
            List<Path> topics = readDirs(workDir);
            boolean needComma = false;
            for (Iterator<Path> iter = topics.iterator(); iter.hasNext(); ) {
                Path cur = iter.next();
                List<String> runs = readSortedRuns(cur.toString());
                if (0 != runs.size()) {
                    String chosen = runs.get(runs.size() - 1);
                    if (needComma) {
                        response += ", " +
                            quote(cur.getFileName().toString()) +
                            ": " + jsonStats(cur.toString(), chosen);
                    } else {
                        response += quote(cur.getFileName().toString()) +
                            ": " + jsonStats(cur.toString(), chosen);
                        needComma = true;
                    }
                }
            }
        } catch (IOException ex) {
            logger.error("stats.json " + ex.toString());
            return json + ", \"error\": " + quote(ex.toString()) + "}";
        }
        logger.debug("stats.json done");
        return response + "}}";
    }

    String processValues(boolean background) {
        String json = "{\"service\": " + quote("values") + ", " +
            "\"dir\": " + quote(workDir);
        String response = json + ", \"result\": {";
        try {
            if (!background) {
                throw new IOException("processing not finished yet");
            }
            List<Path> topics = readDirs(workDir);
            boolean needComma = false;
            for (Iterator<Path> iter = topics.iterator(); iter.hasNext(); ) {
                Path cur = iter.next();
                List<String> runs = readSortedRuns(cur.toString());
                if (0 != runs.size()) {
                    String chosen = runs.get(runs.size() - 1);
                    if (needComma) {
                        response += ", " +
                            quote(cur.getFileName().toString()) +
                            ": " + jsonValues(cur.toString(), chosen);
                    } else {
                        response += quote(cur.getFileName().toString()) +
                            ": " + jsonValues(cur.toString(), chosen);
                        needComma = true;
                    }
                }
            }
        } catch (IOException ex) {
            logger.error("values.json " + ex.toString());
            return json + ", \"error\": " + quote(ex.toString()) + "}";
        }
        logger.debug("values.json done");
        return response + "}}";
    }

    List<Path> readDirs(String directory) throws IOException {
        List<Path> fileNames = new ArrayList<Path>();
        try (DirectoryStream<Path> directoryStream = 
                Files.newDirectoryStream(Paths.get(directory))) {
            for (Path path : directoryStream) {
                fileNames.add(path);
            }
        } catch (IOException ex) {throw ex;}
        return fileNames;
    }

    List<String> readSortedRuns(String directory) throws IOException {
        String history = directory + "/history";
        List<String> fileNames = new ArrayList<String>();
        try (DirectoryStream<Path> directoryStream = 
                Files.newDirectoryStream(Paths.get(history))) {
            for (Path path : directoryStream) {
                fileNames.add(path.getFileName().toString());
            }
        } catch (IOException ex) {throw ex;}
        Collections.sort(fileNames);
        return fileNames;
    }

    String jsonStats(String directory, String run) throws IOException {
        String result = "{\"subdir\": " + quote(run);
        String name = directory + "/history/" + run + "/offsets.csv";
        File file = new File(name);
        try {
            Long totalValue = new Long(0);
            Long counter = new Long(0);
            Long maxValue = new Long(Long.MIN_VALUE);
            Long minValue = new Long(Long.MAX_VALUE);
            List<String> lines = Files.readAllLines(file.toPath(), 
                    StandardCharsets.UTF_8);
            for (String line : lines) {
                if (!line.isEmpty()) {
                    String[] array = line.split(",");
                    if (array.length != 2) {
                        throw new IOException("Error: corrupted file line " +
                                line);
                    }
                    Long partition = new Long(0);
                    Long value = new Long(0);
                    try {
                        partition = Long.parseLong(array[0].trim(), 10);
                        value = Long.parseLong(array[1].trim(), 10);
                    } catch (NumberFormatException ex) {
                        throw new IOException("Error: corrupted file format " +
                                ex.toString());
                    }
                    totalValue += value;
                    maxValue = (value > maxValue ? value : maxValue);
                    minValue = (value < minValue ? value : minValue);
                    counter += 1;
                }
            }
            if (0 != counter) {
                Double meanValue = new Double(totalValue / counter);
                result += ", \"total\": " + quote(totalValue.toString()) +
                        ", \"min\": " + quote(minValue.toString()) +
                        ", \"max\": " + quote(maxValue.toString()) +
                        ", \"mean\": " + quote(meanValue.toString());
            } else {
                Double meanValue = new Double(0.0);
                result += ", \"total\": \"0\", \"min\": \"0\", \"max\": \"0\"" +
                    ", \"mean\": " + quote(meanValue.toString());
            }
        } catch (IOException ex) {throw ex;}
        return result + "}";
    }

    String jsonValues(String directory, String run) throws IOException {
        String result = "{\"subdir\": " + quote(run) + ", \"records\": {";
        String name = directory + "/history/" + run + "/offsets.csv";
        File file = new File(name);
        try {
            List<String> lines = Files.readAllLines(file.toPath(), 
                    StandardCharsets.UTF_8);
            Map<Long, Long> total = new HashMap<Long, Long>();
            for (String line : lines) {
                if (!line.isEmpty()) {
                    String[] array = line.split(",");
                    if (array.length != 2) {
                        throw new IOException("Error: corrupted file line " +
                                line);
                    }
                    try {
                        Long partition = Long.parseLong(array[0].trim(), 10);
                        Long value = Long.parseLong(array[1].trim(), 10);
                        Long cur = total.get(partition);
                        if (null == cur) {
                            total.put(partition, value);
                        } else {
                            total.put(partition, cur + value);
                        }
                    } catch (NumberFormatException ex) {
                        throw new IOException("Error: corrupted file format " +
                                ex.toString());
                    }
                    
                }
            }
            boolean needComma = false;
            Iterator iter = total.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry pair = (Map.Entry)iter.next();
                if (needComma) {
                    result += ", \"" + pair.getKey().toString() + "\": " +
                        "\"" + pair.getValue().toString() + "\"";
                } else {
                    result += "\"" + pair.getKey().toString() + "\": " +
                        "\"" + pair.getValue().toString() + "\"";
                    needComma = true;
                }
            }
        } catch (IOException ex) {throw ex;}
        return result + "}}";
    }

    // Copy-paste from org.codehaus.jettison.json.JSONObject.quote
    String quote(String string) {
        if (string == null || string.length() == 0) {
            return "\"\"";
        }

        char         c = 0;
        int          i;
        int          len = string.length();
        StringBuilder sb = new StringBuilder(len + 4);
        String       t;

        sb.append('"');
        for (i = 0; i < len; i += 1) {
            c = string.charAt(i);
            switch (c) {
            case '\\':
            case '"':
                sb.append('\\');
                sb.append(c);
                break;
            case '/':
 //               if (b == '<') {
                    sb.append('\\');
 //               }
                sb.append(c);
                break;
            case '\b':
                sb.append("\\b");
                break;
            case '\t':
                sb.append("\\t");
                break;
            case '\n':
                sb.append("\\n");
                break;
            case '\f':
                sb.append("\\f");
                break;
            case '\r':
               sb.append("\\r");
               break;
            default:
                if (c < ' ') {
                    t = "000" + Integer.toHexString(c);
                    sb.append("\\u" + t.substring(t.length() - 4));
                } else {
                    sb.append(c);
                }
            }
        }
        sb.append('"');
        return sb.toString();
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MegaFServer.class, args);
    }
}
